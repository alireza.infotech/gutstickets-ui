# gutstickets-ui

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
please pay attention that server MUST be on port 8003
    In case you can't change server port you can change client side server port statically in App.vue file
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
